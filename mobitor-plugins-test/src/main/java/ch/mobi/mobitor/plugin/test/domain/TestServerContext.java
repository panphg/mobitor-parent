package ch.mobi.mobitor.plugin.test.domain;

/*-
 * §
 * mobitor-plugins-test
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.ServerContext;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestServerContext implements ServerContext {

    private final Map<String, List<ApplicationInformation>> appInfoMapByType = new HashMap<>();
    private final Map<String, List<ApplicationInformation>> appInfoMapByName = new HashMap<>();

    @Override
    public List<ApplicationInformation> getMatchingInformation(String type) {
        return appInfoMapByType.get(type);
    }

    @Override
    public void addInformation(String applicationName, ApplicationInformation applicationInformation) {
        List<ApplicationInformation> appInfoListByName = appInfoMapByName.computeIfAbsent(applicationName, k -> new ArrayList<>());
        List<ApplicationInformation> appInfoListByType = appInfoMapByType.computeIfAbsent(applicationInformation.getType(), k -> new ArrayList<>());

        appInfoListByName.add(applicationInformation);
        appInfoListByType.add(applicationInformation);
    }

    @Override
    public List<ApplicationInformation> getMatchingInformation(String type, String applicationName) {
        List<? extends ApplicationInformation> applicationInformationList = appInfoMapByName.get(applicationName);

        List<ApplicationInformation> appInfos = new ArrayList<>();
        for (ApplicationInformation information : applicationInformationList) {
            if (information.getType().equals(type)) {
                appInfos.add(information);
            }
        }

        return appInfos;
    }

    @Override
    public List<ApplicationInformation> getMatchingInformationByApplicationName(String applicationName) {
        List<ApplicationInformation> matchingAppInfos = appInfoMapByName.get(applicationName);
        return Collections.unmodifiableList(matchingAppInfos == null ? new ArrayList<>() : matchingAppInfos);
    }
}
