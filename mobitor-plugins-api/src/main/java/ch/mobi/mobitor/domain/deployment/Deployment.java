package ch.mobi.mobitor.domain.deployment;

/*-
 * §
 * mobitor-plugins-api
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Deployment implements Serializable {

    private String id;
    private String serverName;
    private String environment;
    private String requestUser;
    private Long deploymentDate;
    private String state;

    private List<DeploymentApplication> applications = new ArrayList<>();
    private Map<String, String> parameters = new HashMap<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getRequestUser() {
        return requestUser;
    }

    public void setRequestUser(String requestUser) {
        this.requestUser = requestUser;
    }

    public Long getDeploymentDate() {
        return deploymentDate;
    }

    public void setDeploymentDate(Long deploymentDate) {
        this.deploymentDate = deploymentDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<DeploymentApplication> getApplications() {
        return applications;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void addDeploymentApplication(DeploymentApplication deploymentApplication) {
        this.applications.add(deploymentApplication);
    }

    public void addDeploymentParameter(String key, String value) {
        this.parameters.put(key, value);
    }

    public boolean isSuccessful() {
        return "success".equalsIgnoreCase(this.state);
    }

    public String getFirstApplicationVersion() {
        if (this.applications.size() > 0) {
            return this.applications.get(0).getVersion();
        }
        return null;
    }

    public String getParameter(String key) {
        return this.parameters.get(key);
    }
}
