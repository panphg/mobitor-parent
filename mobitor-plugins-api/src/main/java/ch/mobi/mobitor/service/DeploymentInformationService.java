package ch.mobi.mobitor.service;

/*-
 * §
 * mobitor-plugins-api
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.deployment.Deployment;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Service
public interface DeploymentInformationService {

    List<Deployment> retrieveDeployments(@NotNull String serverName);

    List<Deployment> filterDeployments(@NotNull List<Deployment> deployments, @NotNull Set<String> environments);

    String getVersion(@NotNull String applicationName, @NotNull List<Deployment> deployments);

    String createDeploymentWebUri(@NotNull String serverName, @NotNull String applicationName, @NotNull String environment);

}
