package ch.mobi.mobitor.plugins.api;

/*-
 * §
 * mobitor-plugins-api
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ScreenAttributeProvider;

import java.util.Collections;
import java.util.List;

public interface MobitorPlugin<C> {

    String getConfigPropertyName();

    Class<C> getConfigClass();

    void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<C> configs);

    /**
     * @return a list example application information instances used for the legend shown per plugin on the legend page
     */
    List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList();

    /**
     * if the application information contains configuration settings, a plugin can provide a
     * {@link ScreenAttributeProvider} to pass attributes to the screen so they can be used within the html template.
     *
     * @return a default {@link ScreenAttributeProvider} that returns an empty map
     */
    default ScreenAttributeProvider getScreenAttributeProvider() {
        return Collections::emptyMap;
    }

}
