package ch.mobi.mobitor.metric;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import com.google.common.collect.ImmutableList;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class DefaultCollectorMetricService implements CollectorMetricService {

    private final MeterRegistry meterRegistry;
    // prevent loosing gauges due to GC:
    // https://micrometer.io/docs/concepts#_why_is_my_gauge_reporting_nan_or_disappearing
    // https://github.com/micrometer-metrics/micrometer-docs/issues/23 (follow commit link)
    private Map<String, AtomicLong> gaugeReferences = new HashMap<>();

    // store the last completion date of a collector
    private Map<String, Date> collectorLastRun = new HashMap<>();

    @Autowired
    public DefaultCollectorMetricService(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Override
    public void submitCollectorDuration(@NotNull String collectorName, long durationInMs) {
        if (gaugeReferences.containsKey(collectorName)) {
            // read AtomicLong, set new value, instance is monitored from micrometer
            AtomicLong metricValue = gaugeReferences.get(collectorName);
            metricValue.set(durationInMs);
        } else {
            // register gauge when new, keep reference to settable long:
            Tag collectorTag = Tag.of("collector", collectorName);
            List<Tag> tags = ImmutableList.of(collectorTag);
            AtomicLong metricValue = new AtomicLong(durationInMs);

            AtomicLong gaugedLong = meterRegistry.gauge("gauge_collector_duration_ms", tags, metricValue);
            gaugeReferences.put(collectorName, gaugedLong);
        }
    }

    @Override
    public void updateLastRunCompleted(String infoType) {
        this.collectorLastRun.put(infoType, new Date());
    }

    public Map<String, Date> getCollectorLastRun() {
        return collectorLastRun;
    }

}
