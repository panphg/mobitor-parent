package ch.mobi.mobitor.domain.screen;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DefaultServerContext implements ServerContext {

    /** use applicationName as key for this map, the list will contain information for this application */
    private Map<String, List<ApplicationInformation>> applicationInformationMap = new HashMap<>();

    public DefaultServerContext() {
    }

    public void addInformation(String applicationName, ApplicationInformation information) {
        List<ApplicationInformation> applicationInformationList = this.applicationInformationMap.get(applicationName);
        // lazy initialize the map, since some applicationName's are not known from configuration only and determined during screen creation in the plugin
        if (applicationInformationList == null) {
            applicationInformationList = new ArrayList<>();
            applicationInformationMap.put(applicationName, applicationInformationList);
        }

        applicationInformationList.add(information);
    }

    @Override
    public List<ApplicationInformation> getMatchingInformation(String type, String applicationName) {
        List<ApplicationInformation> applicationInformationList = applicationInformationMap.get(applicationName);

        return applicationInformationList == null ?
                new ArrayList<>() :
                applicationInformationList.stream().filter(info -> info.getType().equals(type)).collect(Collectors.toList());
    }

    @Override
    public List<ApplicationInformation> getMatchingInformationByApplicationName(String applicationName) {
        List<? extends ApplicationInformation> matchingAppInfos = applicationInformationMap.get(applicationName);
        return Collections.unmodifiableList(matchingAppInfos == null ? new ArrayList<>() : matchingAppInfos);
    }

    @Override
    public <T extends ApplicationInformation> List<T> getMatchingInformation(String type) {
        List<ApplicationInformation> allMatchingInfos = new ArrayList<>();

        for (Map.Entry<String, List<ApplicationInformation>> entry : applicationInformationMap.entrySet()) {
            List<ApplicationInformation> matchingInformation = getMatchingInformation(type, entry.getKey());
            allMatchingInfos.addAll(matchingInformation);
        }

        return (List<T>) allMatchingInfos;
    }

}
