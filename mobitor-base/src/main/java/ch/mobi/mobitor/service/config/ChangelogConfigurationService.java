package ch.mobi.mobitor.service.config;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.config.ChangelogConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class ChangelogConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(ChangelogConfigurationService.class);

    private final ResourceLoader resourceLoader;
    private List<ChangelogConfig> changelogConfigs;
    private Date changelogRefresh;

    @Autowired
    public ChangelogConfigurationService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @PostConstruct
    public void initializeChangelogRepositories() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        Resource resource = resourceLoader.getResource("classpath:screen/changelog-repositories.json");
        try (InputStream inputStream = resource.getInputStream()) {
            this.changelogConfigs = mapper.readValue(inputStream, mapper.getTypeFactory().constructCollectionType(List.class, ChangelogConfig.class));

        } catch (Exception e) {
            LOG.error("Could not initialize changelog configuration / repositories.", e);
        }

    }

    public List<ChangelogConfig> getAllChangelogConfigs() {
        return Collections.unmodifiableList(changelogConfigs);
    }

    public void setChangelogRefresh(Date changelogRefresh) {
        this.changelogRefresh = changelogRefresh;
    }

    public Date getChangelogRefresh() {
        return changelogRefresh;
    }
}
