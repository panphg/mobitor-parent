package ch.mobi.mobitor;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2019 - 2020 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class SwaggerRedirect {
// Fixes security issue with springfox-swagger-ui
// https://github.com/springfox/springfox/issues/3193#issuecomment-558748502

    @Value("${springfox.documentation.swagger.v2.path:/v2/api-docs}")
    private String springfoxPath;

    @GetMapping("/swagger-ui.html")
    public void redirectToUi(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String contextPath = request.getContextPath();
        final String url = "?url=" + contextPath + springfoxPath;
        final String configUrl = "&configUrl=" + contextPath + "/swagger-resources/configuration/ui";
        response.sendRedirect("webjars/swagger-ui/index.html" + url + configUrl);
    }
}
