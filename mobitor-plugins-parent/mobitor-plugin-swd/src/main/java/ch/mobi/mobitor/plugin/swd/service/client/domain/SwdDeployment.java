package ch.mobi.mobitor.plugin.swd.service.client.domain;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class SwdDeployment {

    @JsonProperty("Name") private String environment;

    @JsonProperty("Patches") private List<SwdPatch> patches = new LinkedList<>();

    public String getEnvironment() {
        return environment;
    }

    void setEnvironment(String environment) {
        this.environment = environment;
    }

    public List<SwdPatch> getPatches() {
        return patches;
    }

    void addPatch(SwdPatch patch) {
        this.patches.add(patch);
    }

    public Optional<SwdPatch> findPatchByFileName(String fileName) {
        Objects.requireNonNull(fileName, "Filename must not be null!");
        return patches.stream()
                      .filter(d -> fileName.equals(d.getFileName()))
                      .reduce((a, b) -> {
                          throw new IllegalStateException("Multiple patches for fileName " + fileName + " found: " + a + ", " + b);
                      });
    }

    @Override
    public String toString() {
        return "SwdDeployment{" +
                "environment='" + environment + '\'' +
                ", patches=" + patches +
                '}';
    }
}
