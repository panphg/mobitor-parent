package ch.mobi.mobitor.plugin.swd.service.client.domain;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

public class SwdPatchTest {

    @NotNull
    public static SwdPatch aPatch(String fileName) {
        return aPatch(fileName, null, null);
    }

    public static SwdPatch aPatch(String fileName, String revision, LocalDateTime patchDate) {
        SwdPatch patch = new SwdPatch();
        patch.setFileName(fileName);
        patch.setRevision(revision);
        patch.setDate(patchDate == null ? null : patchDate.toLocalDate());
        patch.setTime(patchDate == null ? null : patchDate.toLocalTime());
        return patch;
    }

}
