package ch.mobi.mobitor.plugin.zaproxy.service.client;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugin.zaproxy.client.domain.ProjectComponent;
import ch.mobi.mobitor.plugin.zaproxy.client.domain.TaskList;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.MessageFormat;

@Component
public class ZaproxySonarQubeClient {

    private static final Logger LOG = LoggerFactory.getLogger(ZaproxySonarQubeClient.class);

    private final ZaproxySonarQubeConfiguration sonarQubeConfiguration;

    @Autowired
    public ZaproxySonarQubeClient(ZaproxySonarQubeConfiguration sonarQubeConfiguration) {
        this.sonarQubeConfiguration = sonarQubeConfiguration;
    }

    public ProjectComponent retrieveProjectInformation(String projectKey) {
        final String sonarQubeHost = sonarQubeConfiguration.getBaseUrl();

        String requestUri = MessageFormat.format("{0}/api/measures/component?metricKeys=blocker_violations,critical_violations,major_violations,minor_violations,info_violations&component={1}", sonarQubeHost, projectKey);

        try {
            Executor executor = Executor.newInstance();
            String response = executor.execute(
                    Request.Get(requestUri)
                           .addHeader("accept", "application/json")
                           .connectTimeout(1000)
                           .socketTimeout(1000)
            )
                                      .returnContent()
                                      .asString();

            return createSonarQubeResponse(response);

        } catch (IOException ex) {
            LOG.error("Could not retrieve Zaproxy metrics from SonarQube: " + projectKey);
        }

        return null;
    }

    public TaskList retrieveTaskList(String projectKey) {
        final String sonarQubeHost = sonarQubeConfiguration.getBaseUrl();

        String requestUri = MessageFormat.format("{0}/api/ce/component?component={1}", sonarQubeHost, projectKey);

        try {
            Executor executor = Executor.newInstance();
            String response = executor.execute(
                    Request.Get(requestUri)
                           .addHeader("accept", "application/json")
                           .connectTimeout(1000)
                           .socketTimeout(1000)
            )
                                      .returnContent()
                                      .asString();

            return createTaskList(response);

        } catch (IOException ex) {
            LOG.error("Could not retrieve Zaproxy metrics from SonarQube: " + projectKey);
        }

        return null;
    }

    private ProjectComponent createSonarQubeResponse(String jsonResponse) throws IOException {
        if (jsonResponse == null) {
            return null;
        }

        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        return mapper.readValue(jsonResponse, MetricResponse.class).component;
    }

    private TaskList createTaskList(String jsonResponse) throws IOException {
        if (jsonResponse == null) {
            return null;
        }

        ObjectMapper mapper = new ObjectMapper();
        //mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        return mapper.readValue(jsonResponse, TaskList.class);
    }

    /**
     * Helper class. The useful JSON - Part is wrapped inside this property.
     */
    static class MetricResponse {
        @JsonProperty
        ProjectComponent component;
    }

    static class TaskListResponse {
        @JsonProperty
        TaskList list;
    }
}
