package ch.mobi.mobitor.plugin.zaproxy.client.domain;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

public class TaskInformation {

	@JsonProperty
	private String id;
	@JsonProperty
	private String type;
	@JsonProperty
	private String componentId;
	@JsonProperty
	private String componentKey;
	@JsonProperty
	private String componentName;
	@JsonProperty
	private String componentQualifier;
	@JsonProperty
	private String analysisId;
	@JsonProperty
	private String status;
	@JsonProperty
	private String submittedAt;
	@JsonProperty
	private String startedAt;
	@JsonProperty
	private String executedAt;
	@JsonProperty
	private String executionTimeMs;
	@JsonProperty
	private String logs;
	@JsonProperty
	private String hasScannerContext;
	@JsonProperty
	private String organization;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public String getComponentKey() {
		return componentKey;
	}

	public void setComponentKey(String componentKey) {
		this.componentKey = componentKey;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getComponentQualifier() {
		return componentQualifier;
	}

	public void setComponentQualifier(String componentQualifier) {
		this.componentQualifier = componentQualifier;
	}

	public String getAnalysisId() {
		return analysisId;
	}

	public void setAnalysisId(String analysisId) {
		this.analysisId = analysisId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubmittedAt() {
		return submittedAt;
	}

	public void setSubmittedAt(String submittedAt) {
		this.submittedAt = submittedAt;
	}

	public String getStartedAt() {
		return startedAt;
	}

	public void setStartedAt(String startedAt) {
		this.startedAt = startedAt;
	}

	public String getExecutedAt() {
		return executedAt;
	}

	public void setExecutedAt(String executedAt) {
		this.executedAt = executedAt;
	}

	public String getExecutionTimeMs() {
		return executionTimeMs;
	}

	public void setExecutionTimeMs(String executionTimeMs) {
		this.executionTimeMs = executionTimeMs;
	}

	public String getLogs() {
		return logs;
	}

	public void setLogs(String logs) {
		this.logs = logs;
	}

	public String getHasScannerContext() {
		return hasScannerContext;
	}

	public void setHasScannerContext(String hasScannerContext) {
		this.hasScannerContext = hasScannerContext;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	@JsonCreator
	public TaskInformation() {
	}

	// Utility methods

	public enum Status {
		SUCCESS, PENDING, FAILED
	}

	public boolean isStatus(TaskInformation.Status status) {
		return status.name().equals(this.status);
	}
}
