package ch.mobi.mobitor.plugin.jira;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.jira.config.JiraFilterConfig;
import ch.mobi.mobitor.plugin.jira.domain.JiraInformation;
import ch.mobi.mobitor.plugin.jira.service.JiraPluginAttributeProvider;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ScreenAttributeProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.jiraFilters.enabled", havingValue = "true")
public class JiraPlugin implements MobitorPlugin<JiraFilterConfig> {

    private final JiraPluginAttributeProvider screenAttributeProvider;

    @Autowired
    public JiraPlugin(JiraPluginAttributeProvider screenAttributeProvider) {
        this.screenAttributeProvider = screenAttributeProvider;
    }

    @Override
    public String getConfigPropertyName() {
        return "jiraFilters";
    }

    @Override
    public Class<JiraFilterConfig> getConfigClass() {
        return JiraFilterConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<JiraFilterConfig> configs) {
        for (JiraFilterConfig jiraFilterConfig : configs) {
            String serverName = jiraFilterConfig.getServerName();
            String amwApplicationName = jiraFilterConfig.getApplicationName();
            String env = jiraFilterConfig.getEnvironment();

            JiraInformation information = new JiraInformation();
            information.setFilterId(jiraFilterConfig.getFilterId());

            screen.addInformation(serverName, amwApplicationName, env, information);
        }
    }

    @Override
    public ScreenAttributeProvider getScreenAttributeProvider() {
        return screenAttributeProvider;
    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new JiraInformationLegendGenerator().getLegendList();
    }
}
