package ch.mobi.mobitor.plugin.kubernetes.service.client.domain;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @see <a href="https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#containerstatus-v1-core">ContainerStatus v1 core</a>
 */
public class ContainerStatus {

    @JsonProperty
    private String name; // service name, quite reliable

    @JsonProperty
    private boolean ready;

    @JsonProperty
    private int restartCount = -1;

    @JsonProperty
    private String image;

    @JsonProperty
    private ContainerState state;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public int getRestartCount() {
        return restartCount;
    }

    public void setRestartCount(int restartCount) {
        this.restartCount = restartCount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ContainerState getState() {
        return state;
    }

    public void setState(ContainerState state) {
        this.state = state;
    }
}
