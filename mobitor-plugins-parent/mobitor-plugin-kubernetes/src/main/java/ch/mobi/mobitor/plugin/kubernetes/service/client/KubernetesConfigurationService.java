package ch.mobi.mobitor.plugin.kubernetes.service.client;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.KubernetesServerConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

@Service
public class KubernetesConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(KubernetesConfigurationService.class);

    private final ResourceLoader resourceLoader;

    private List<KubernetesServerConfig> kubernetesServerConfigs;

    @Autowired
    public KubernetesConfigurationService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @PostConstruct
    public void initializeKubernetesServers() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        Resource resource = resourceLoader.getResource("classpath:mobitor/plugin/kubernetes/config/kubernetes-master-servers.json");
        try (InputStream inputStream = resource.getInputStream()) {
            kubernetesServerConfigs = mapper.readValue(inputStream, mapper.getTypeFactory().constructCollectionType(List.class, KubernetesServerConfig.class));

        } catch (Exception e) {
            LOG.error("Could not initialize kubernetes servers.", e);
        }
    }

    public List<KubernetesServerConfig> getKubernetesServerConfigs() {
        return Collections.unmodifiableList(kubernetesServerConfigs);
    }

    public KubernetesServerConfig getKubernetesServerConfigForEnvironment(String environment) {
        for (KubernetesServerConfig kubernetesServerConfig : kubernetesServerConfigs) {
            if (kubernetesServerConfig.getEnvironments().contains(environment)) {
                return kubernetesServerConfig;
            }
        }
        return null;
    }

    public KubernetesServerConfig getKubernetesServerConfigForNetwork(EnvironmentNetwork network) {
        for (KubernetesServerConfig kubernetesServerConfig : kubernetesServerConfigs) {
            if (kubernetesServerConfig.getNetwork().equals(network)) {
                return kubernetesServerConfig;
            }
        }
        return null;
    }
}
