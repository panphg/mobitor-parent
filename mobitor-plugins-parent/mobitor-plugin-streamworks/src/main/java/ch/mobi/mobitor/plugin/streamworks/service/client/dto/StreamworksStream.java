package ch.mobi.mobitor.plugin.streamworks.service.client.dto;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 - 2020 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.streamworks.StreamworksConstant;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksState;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.Objects;

import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksState.*;

public class StreamworksStream {
    @JsonProperty("StreamName")
    private String streamName;
    @JsonProperty("RunNumber")
    private Long runNumber;
    @JsonProperty("StreamRunStatus")
    private String streamRunStatus;
    @JsonProperty("StreamStartDateTime")
    private LocalDateTime streamStartDateTime;
    @JsonProperty("StreamEndDateTime")
    private LocalDateTime streamEndDateTime;
    @JsonProperty("DocumentationLink")
    private String documentationLink;
    @JsonProperty("RuntimeLink")
    private String runtimeLink;


    // Constructor for Jackson deserializer
    public StreamworksStream() {
    }

    public StreamworksStream(String streamName,
                             Long runNumber,
                             String streamRunStatus,
                             String streamStartDateTime,
                             String streamEndDateTime,
                             String documentationLink,
                             String runtimeLink) {
        this.streamName = streamName;
        this.runNumber = runNumber;
        this.streamRunStatus = streamRunStatus;
        this.streamStartDateTime = LocalDateTime.parse(streamStartDateTime, StreamworksConstant.INPUT_FORMATTER);
        this.streamEndDateTime = LocalDateTime.parse(streamEndDateTime, StreamworksConstant.INPUT_FORMATTER);
        this.documentationLink = documentationLink;
        this.runtimeLink = runtimeLink;
    }


    public String getStreamName() {
        return streamName;
    }

    public Long getRunNumber() {
        return runNumber;
    }

    public String getStreamRunStatus() {
        return streamRunStatus;
    }

    public LocalDateTime getStreamStartDateTime() {
        return streamStartDateTime;
    }

    public String getParsedStartDateTime() {
        if (streamStartDateTime == null) return "";
        return streamStartDateTime.format(StreamworksConstant.FORMATTER);
    }

    public LocalDateTime getStreamEndDateTime() {
        return streamEndDateTime;
    }

    public String getParsedEndDateTime() {
        if (streamEndDateTime == null) return "";
        return streamEndDateTime.format(StreamworksConstant.FORMATTER);
    }

    public String getDocumentationLink() {
        return documentationLink;
    }

    public String getRuntimeLink() {
        return runtimeLink;
    }

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }

    public void setRunNumber(Long runNumber) {
        this.runNumber = runNumber;
    }

    public void setStreamRunStatus(String streamRunStatus) {
        this.streamRunStatus = streamRunStatus;
    }

    public String getStatusIcon() {
        switch (streamRunStatus) {
            case "Prepared":
                return "fa fa-align-justify";
            case "Running":
                return "fa fa-spinner fa-spin";
            case "AbnormallyEnded":
                return "fa fa-exclamation-triangle";
            case "Completed":
                // "fa fa-check-circle"
                return "fa fa-thumbs-up";
            default:
                return "fa fa-question-circle";
        }
    }

    public String getStatusStyle() {
        switch (streamRunStatus) {
            case "Prepared":
                return "color:yellow;";
            case "Running":
                return "color:DeepSkyBlue;";
            case "AbnormallyEnded":
                return "color:OrangeRed;";
            case "Completed":
                return "color:SpringGreen;";
            default:
                return "color:orange;";
        }
    }

    public void setStreamStartDateTime(String streamStartDateTime) {
        if (streamStartDateTime != null && !streamStartDateTime.isEmpty())
            this.streamStartDateTime = LocalDateTime.parse(streamStartDateTime, StreamworksConstant.INPUT_FORMATTER);
    }

    public void setStreamEndDateTime(String streamEndDateTime) {
        if (streamEndDateTime != null && !streamEndDateTime.isEmpty())
            this.streamEndDateTime = LocalDateTime.parse(streamEndDateTime, StreamworksConstant.INPUT_FORMATTER);
    }

    public void setDocumentationLink(String documentationLink) {
        this.documentationLink = documentationLink;
    }

    public void setRuntimeLink(String runtimeLink) {
        this.runtimeLink = runtimeLink;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        StreamworksStream that = (StreamworksStream) obj;
        return streamName.equals(that.streamName) &&
                documentationLink.equals(that.documentationLink) &&
                runtimeLink.equals(that.runtimeLink) &&
                runNumber.equals(that.runNumber) &&
                streamRunStatus.equals(that.streamRunStatus) &&
                Objects.equals(streamStartDateTime, that.streamStartDateTime) &&
                Objects.equals(streamEndDateTime, that.streamEndDateTime);
    }

    @Override
    public String toString() {
        return "StreamworksStream{" +
                "streamName='" + streamName + '\'' +
                ", runNumber=" + runNumber +
                ", streamRunStatus='" + streamRunStatus + '\'' +
                ", streamStartDateTime=" + getParsedStartDateTime() +
                ", streamEndDateTime=" + getParsedEndDateTime() +
                ", documentationLink='" + documentationLink + '\'' +
                ", runtimeLink='" + runtimeLink + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(streamName, runNumber, streamRunStatus, streamStartDateTime, streamEndDateTime, documentationLink, runtimeLink);
    }

}
