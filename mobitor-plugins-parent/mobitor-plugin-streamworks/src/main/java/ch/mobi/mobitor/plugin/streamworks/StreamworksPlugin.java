package ch.mobi.mobitor.plugin.streamworks;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.streamworks.config.StreamworksConfig;
import ch.mobi.mobitor.plugin.streamworks.config.StreamworksLastRunConfig;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksStatusInformation;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.jetbrains.annotations.TestOnly;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.streamworks.enabled", havingValue = "true")
public class StreamworksPlugin implements MobitorPlugin<StreamworksConfig> {

    private final EnvironmentsConfigurationService environmentsConfigurationService;

    @Autowired
    public StreamworksPlugin(EnvironmentsConfigurationService environmentsConfigurationService) {
        this.environmentsConfigurationService = environmentsConfigurationService;
    }

    @Override
    public String getConfigPropertyName() {
        return "streamworksStatus";
    }

    @Override
    public Class<StreamworksConfig> getConfigClass() {
        return StreamworksConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<StreamworksConfig> configs) {
        String env;
        for (StreamworksConfig config : configs) {
            env = config.getEnvironment();
            StreamworksStatusInformation info = createStreamworksStatusInformation(config.getProcess(), config.getLabel(), env);
            screen.addInformation(config.getServerName(), config.getApplicationName(), env, info);
        }
    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new StreamworksStatusLegendGenerator().getLegendList();
    }

    @TestOnly
    StreamworksStatusInformation createStreamworksStatusInformation(String applicationName, String label, String env) {
        return new StreamworksStatusInformation(applicationName, label, env);
    }
}
