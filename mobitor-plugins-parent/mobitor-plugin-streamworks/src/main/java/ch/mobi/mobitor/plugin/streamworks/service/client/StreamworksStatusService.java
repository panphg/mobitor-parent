package ch.mobi.mobitor.plugin.streamworks.service.client;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.WebClients;
import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksServerConfig;
import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksStream;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class StreamworksStatusService {
    private static final Logger LOG = LoggerFactory.getLogger(StreamworksStatusService.class);
    private static final int ONE_MONTH = 30; // days

    private static Pattern STATUS_PATTERN = Pattern.compile("Completed|Running|AbnormallyEnded");

    private final StreamworksStatusConfigurationService configurationService;

    @Autowired
    public StreamworksStatusService(StreamworksStatusConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Nullable
    public StreamworksStatus retrieveStreamworksInformation(String serverName, String environment) {
        StreamworksServerConfig config = configurationService.getStreamworksServerConfig(environment);
        if (config != null) {
            LocalDate dt = LocalDate.now();
            String result;
            String url = config.getUrl() + "/" + serverName + "/";
            try {
                for (int dtQty = 0; dtQty <= ONE_MONTH; dtQty++) {
                    try {
                        result = WebClients.withKeyStore().get()
                                .uri(url + dt.minusDays(dtQty).format(DateTimeFormatter.ISO_LOCAL_DATE))
                                .retrieve()
                                .bodyToMono(String.class)
                                .block();
                        if (result != null && STATUS_PATTERN.matcher(result).find())
                            return mapStreamWorksStatus(result);
                    } catch (WebClientResponseException.NotFound ex) {
                        LOG.debug("Could not read streamworks state for a specific date from {}", url, ex);
                    }
                }
            } catch (Exception ex) {
                LOG.error("Could not read streamworks state from {}", url, ex);
            }
        }
        return null;
    }

    private StreamworksStatus mapStreamWorksStatus(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        JsonNode streamNode = mapper.readTree(json).path("Streams");
        final String verarbeitung = mapper.readTree(json).path("Verarbeitung").asText();
        final String planDateStr = mapper.readTree(json).path("PlanDate").asText();
        final String status = mapper.readTree(json).path("Status").asText();
        List<StreamworksStream> streamworksStreams = null;
        final String lastUpdateStr = mapper.readTree(json).path("LastUpdate").asText();

        ObjectReader reader = mapper.readerFor(new TypeReference<List<StreamworksStream>>() {
        });

        if (streamNode != null && "ARRAY".equals(streamNode.getNodeType().toString())) {
            streamworksStreams = reader.readValue(streamNode);
        }

        final LocalDate planDate = LocalDate.parse(planDateStr, DateTimeFormatter.ISO_LOCAL_DATE);
        final LocalDateTime lastUpdate =
                LocalDateTime.ofInstant(
                        Instant.from(DateTimeFormatter.ISO_INSTANT.parse(lastUpdateStr)),
                        ZoneId.of("UTC+0")
                );
        return new StreamworksStatus(verarbeitung, planDate, status, streamworksStreams, lastUpdate);
    }


}
