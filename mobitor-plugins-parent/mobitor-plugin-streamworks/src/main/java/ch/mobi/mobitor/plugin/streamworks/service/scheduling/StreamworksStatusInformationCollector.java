package ch.mobi.mobitor.plugin.streamworks.service.scheduling;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.streamworks.StreamworksPlugin;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksState;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksStatusInformation;
import ch.mobi.mobitor.plugin.streamworks.service.client.StreamworksStatus;
import ch.mobi.mobitor.plugin.streamworks.service.client.StreamworksStatusService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.Date;

import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksState.*;
import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksStatusInformation.STREAMWORKS_STATUS;

@Component
@ConditionalOnBean(StreamworksPlugin.class)
public class StreamworksStatusInformationCollector {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final ScreensModel screensModel;
    private final RuleService ruleService;
    private final StreamworksStatusService streamworksStatusService;
    private final CollectorMetricService collectorMetricService;

    @Autowired
    public StreamworksStatusInformationCollector(ScreensModel screensModel,
                                                 RuleService ruleService,
                                                 StreamworksStatusService streamworksStatusService,
                                                 CollectorMetricService collectorMetricService) {
        this.screensModel = screensModel;
        this.ruleService = ruleService;
        this.streamworksStatusService = streamworksStatusService;
        this.collectorMetricService = collectorMetricService;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.streamworksStatusInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectStreamworksStatusInformation() {
        long start = System.currentTimeMillis();
        logger.info("Started retrieving Streamworks Status information...");
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateStreamworksStatusInformation);
        long stop = System.currentTimeMillis();
        long duration = stop - start;
        logger.info("Reading Streamworks Status information took: " + duration + "ms");
        collectorMetricService.submitCollectorDuration("streamworks.status", duration);
        collectorMetricService.updateLastRunCompleted(STREAMWORKS_STATUS);
    }

    private void populateStreamworksStatusInformation(Screen screen) {
        screen.<StreamworksStatusInformation>getMatchingInformation(STREAMWORKS_STATUS).forEach(this::updateInformation);
        ruleService.updateRuleEvaluation(screen, STREAMWORKS_STATUS);
        screen.setRefreshDate(STREAMWORKS_STATUS, new Date());
    }

    private void updateInformation(StreamworksStatusInformation info) {
        StreamworksStatus status = streamworksStatusService.retrieveStreamworksInformation(info.getApplicationName(), info.getEnvironment());
        if (status != null) {
            info.setState(convertStatus(status.getStatus()));
            info.setStreamList(status.getStreamworksStreams());
        }
    }

    private StreamworksState convertStatus(String status) {
        switch (status) {
            case "Prepared":
                return PREPARED;
            case "Running":
                return RUNNING;
            case "AbnormallyEnded":
                return ABNORMALLY_ENDED;
            case "Completed":
                return COMPLETED;
            default:
                return UNKNOWN;
        }
    }
}
