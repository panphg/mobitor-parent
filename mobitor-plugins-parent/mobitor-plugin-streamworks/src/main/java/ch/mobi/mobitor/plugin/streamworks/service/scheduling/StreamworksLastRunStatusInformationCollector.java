package ch.mobi.mobitor.plugin.streamworks.service.scheduling;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.streamworks.StreamworksLastRunPlugin;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunState;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation;
import ch.mobi.mobitor.plugin.streamworks.service.client.StreamworksLastRunStatus;
import ch.mobi.mobitor.plugin.streamworks.service.client.StreamworksLastRunStatusService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunState.*;
import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation.STREAMWORKS_LAST_RUN_STATUS;

@Component
@ConditionalOnBean(StreamworksLastRunPlugin.class)
public class StreamworksLastRunStatusInformationCollector {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final ScreensModel screensModel;
    private final RuleService ruleService;
    private final StreamworksLastRunStatusService streamworksLastRunStatusService;
    private final CollectorMetricService collectorMetricService;

    @Autowired

    public StreamworksLastRunStatusInformationCollector(ScreensModel screensModel,
                                                        RuleService ruleService,
                                                        StreamworksLastRunStatusService streamworksLastRunStatusService,
                                                        CollectorMetricService collectorMetricService) {
        this.screensModel = screensModel;
        this.ruleService = ruleService;
        this.streamworksLastRunStatusService = streamworksLastRunStatusService;
        this.collectorMetricService = collectorMetricService;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.streamworksLastRunStatusInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectStreamworksLastRunStatusInformation() {
        long start = System.currentTimeMillis();
        logger.info("Started retrieving Streamworks Status information...");
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateStreamworksLastRunStatusInformation);
        long stop = System.currentTimeMillis();
        long duration = stop - start;
        logger.info("Reading Streamworks Status information took: " + duration + "ms");
        collectorMetricService.submitCollectorDuration("streamworks.status", duration);
        collectorMetricService.updateLastRunCompleted(STREAMWORKS_LAST_RUN_STATUS);
    }

    private void populateStreamworksLastRunStatusInformation(Screen screen) {
        screen.<StreamworksLastRunStatusInformation>getMatchingInformation(STREAMWORKS_LAST_RUN_STATUS).forEach(this::updateInformation);
        ruleService.updateRuleEvaluation(screen, STREAMWORKS_LAST_RUN_STATUS);
        screen.setRefreshDate(STREAMWORKS_LAST_RUN_STATUS, new Date());
    }

    private void updateInformation(StreamworksLastRunStatusInformation info) {
        StreamworksLastRunStatus status = streamworksLastRunStatusService.retreiveStreamworksInformation(info.getApplicationName(), info.getEnvironment());
        if (status != null) {
            info.setState(convertStatus(status.getStatus()));
            info.setVerarbeitung(info.getVerarbeitung());
            info.setJobList(status.getJobs());
        }
    }

    private StreamworksLastRunState convertStatus(String status) {
        switch (status) {

            case "Running":
                return RUNNING;
            case "AbnormallyEnded":
                return ABNORMALLY_ENDED;
            case "Completed":
                return COMPLETED;
            default:
                return UNKNOWN;
        }
    }
}
