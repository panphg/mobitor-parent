= Mobitor Streamworks Plugin

link:../../index.html[Back Mobitor Overview]

== Features

This plugin retrieves streamworks stream status information from a REST service.

The service the plugin queries is configured in `classpath:mobitor/plugin/streamworks/config/streamworks.json`


== Configuration

.screen.json
[source,json]
----
"streamworksStatus": [
  {
    "server": {"EDWHC": "edwh"},
    "applicationName": "stream"
  }
----

.Parameters
|===
| Name | Description | Required | Default

| server
| Maps the application name to streamwork's "Verarbeitung".
    The server name refers to the serverNames within the same screen, used to reference the position of this information block in the screen
| yes
| empty

| applicationName
| The application name to define and refer to the sub-row of the server name in the same screen. Used to reference the position if this information block in the screen
| no
| STREAMWORKS

|===

The Plugin will show always the Stream status of the current day.

== Result

The plugin fills information blocks that provide job information on a screen with a flag icon:

image::streamworks-information-block.png[]

== Streamworks information per Component

== Configuration
.screen.json
[source,json]
----
"streamworksLastRunStatus" : [
  {
    "streamworksApplicationName": "mps-core-javabatch",
    "label": "mps batch",
    "serverName": "mps_batch",
    "applicationName": "ch_mobi_mps_javabatch",
    "environment": "P"
  }
----
.Parameters
|===
| Name | Description | Required | Default

| server
| Maps the application name to streamwork's "Verarbeitung".
| yes
|
| label
| Label name for the button
| no
|
| serverName
| Maps the label to LiimaServers configured in screen.json
| yes
|
| applicationName
| mapps to the Liima application name configured in screen.json
| yes
|
| environment
| Indicates in which in which column the button should appear
| yes
|


|===

The Plugin configuration shows the last or current jobs of a certain application.

