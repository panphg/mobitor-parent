package ch.mobi.mobitor.plugin.sonarqube.service.client;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.assertj.core.api.Assertions.assertThat;


public class SonarQubeProjectInformationProviderServiceTest {

    public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

    private SonarQubeClient client;
    private CoverageMetricService metricService;

    @BeforeAll
    static void setup() {
        wiremock.start();
    }

    @BeforeEach
    void setupEach() {
        String baseUrl = "http://localhost:" + wiremock.port();
        SonarQubeConfiguration sonarConf = new SonarQubeConfiguration();
        sonarConf.setBaseUrl(baseUrl);

        client = new SonarQubeClient(sonarConf);
        metricService = Mockito.mock(CoverageMetricService.class);
    }

    @AfterEach
    void after() {
        wiremock.resetAll();
    }

    @AfterAll
    static void clean() {
        wiremock.shutdown();
    }

    @Test
    public void updateProjectInformation() {
        // arrange
        final String projectKey = "ch.mobi.ovn:ovn";

        wiremock.stubFor(get(urlMatching("/api/measures/component?(.*)"))
                .withQueryParam("component", equalTo(projectKey))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("sonar-report-response-app2.json")
                )
        );

        // act
        SonarQubeProjectInformationProviderService service = new SonarQubeProjectInformationProviderService(client, metricService);
        SonarInformation sonarInfo = new SonarInformation(projectKey, false);
        service.updateProjectInformation(sonarInfo);

        // assert
        assertThat(sonarInfo).isNotNull();
        assertThat(sonarInfo.getLineCoverage()).isNotNull();
        assertThat(sonarInfo.getDuplicatedBlocks()).isGreaterThan(1f);
    }

}
