package ch.mobi.mobitor.plugin.sonarqube.service.scheduling;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.sonarqube.SonarQubePlugin;
import ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation;
import ch.mobi.mobitor.plugin.sonarqube.service.client.SonarQubeProjectInformationProviderService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation.SONAR;

@Component
@ConditionalOnBean(SonarQubePlugin.class)
public class SonarInformationCollector {

    private static final Logger LOG = LoggerFactory.getLogger(SonarInformationCollector.class);

    private final ScreensModel screensModel;
    private final SonarQubeProjectInformationProviderService sonarQubeProjectInformationProviderService;
    private final RuleService ruleService;

    @Autowired
    public SonarInformationCollector(SonarQubeProjectInformationProviderService sonarQubeProjectInformationProviderService,
                                     RuleService ruleService,
                                     ScreensModel screensModel) {
        this.sonarQubeProjectInformationProviderService = sonarQubeProjectInformationProviderService;
        this.ruleService = ruleService;
        this.screensModel = screensModel;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.sonarInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectSonarQubeProjectInformation() {
        long start = System.currentTimeMillis();
        LOG.info("Started retrieving SonarQube build information...");
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateSonarQubeInformation);
        long stop = System.currentTimeMillis();
        LOG.info("Reading SonarQube information took: " + (stop - start) + "ms");
    }

    private void populateSonarQubeInformation(Screen screen) {
        List<SonarInformation> sonarInformationList = screen.getMatchingInformation(SONAR);
        sonarInformationList.forEach(sonarQubeProjectInformationProviderService::updateProjectInformation);

        ruleService.updateRuleEvaluation(screen, SONAR);
        screen.setRefreshDate(SONAR, new Date());
    }

}
