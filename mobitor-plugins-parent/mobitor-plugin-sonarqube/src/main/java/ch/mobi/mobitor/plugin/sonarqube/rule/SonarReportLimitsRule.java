package ch.mobi.mobitor.plugin.sonarqube.rule;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.domain.screen.RuleViolationSeverity;
import ch.mobi.mobitor.domain.screen.ServerContext;
import ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation.SONAR;

/**
 * Verify that the information from SonarQube is within defined limits.
 *
 * Refers tu rule #2 and #4 of 'Prinzipien für Continuous Delivery'.
 */
@Component
public class SonarReportLimitsRule implements PipelineRule {

    public static final int DEFAULT_CRITICAL_LIMIT = 0;
    public static final float DEFAULT_COVERAGE_LIMIT = 81;
    public static final int DEFAULT_BLOCKER_LIMIT = 0;

    @Override
    public void evaluateRule(Pipeline pipeline, RuleEvaluation newRuleEvaluation) {
        Map<String, ServerContext> serverContextMap = pipeline.getServerContextMap();
        for (Map.Entry<String, ServerContext> entry : serverContextMap.entrySet()) {
            String env = entry.getKey();
            ServerContext serverContext = entry.getValue();

            List<SonarInformation> sonarInformationList = serverContext.getMatchingInformation(SONAR);

            sonarInformationList.stream().filter(this::ruleViolated).forEach(sonarInformation -> newRuleEvaluation.addViolation(env, sonarInformation, RuleViolationSeverity.ERROR, "Sonar Report unhappy."));
        }
    }

    @Override
    public boolean validatesType(String type) {
        return SONAR.equals(type);
    }

    public boolean ruleViolated(SonarInformation sonarInformation) {
        boolean violated;

        float coverageLimit = !sonarInformation.getStrictViolations() && sonarInformation.getCoverageLimit() != null ? sonarInformation.getCoverageLimit() : DEFAULT_COVERAGE_LIMIT;
        int criticalLimit = !sonarInformation.getStrictViolations() && sonarInformation.getCriticalLimit() != null ? sonarInformation.getCriticalLimit() : DEFAULT_CRITICAL_LIMIT;
        int blockerLimit = !sonarInformation.getStrictViolations() && sonarInformation.getBlockerLimit() != null ? sonarInformation.getBlockerLimit() : DEFAULT_BLOCKER_LIMIT;

        violated = (sonarInformation.getBlockers() > blockerLimit)
                || (sonarInformation.getCriticals() > criticalLimit)
                || (sonarInformation.getViolationsCoverage() < coverageLimit);

        return violated;
    }

}
