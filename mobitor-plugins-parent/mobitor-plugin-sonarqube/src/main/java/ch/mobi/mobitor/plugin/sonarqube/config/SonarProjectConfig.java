package ch.mobi.mobitor.plugin.sonarqube.config;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugins.api.PluginConfig;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SonarProjectConfig extends PluginConfig {

    /**
     * typically the maven groupId:artifactId
     *
     * ex:
     *  - ch.mobi.vvn.aad:vvn_aad
     *  - ch.mobi.vvn.jeeservice:vvn
     */
    @JsonProperty private String projectKey;
    @JsonProperty private boolean strictViolations = false;
    @JsonProperty private Float coverageLimit;
    @JsonProperty private Integer blockerLimit;
    @JsonProperty private Integer criticalLimit;

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public boolean getStrictViolations() {
        return strictViolations;
    }

    public void setStrictViolations(boolean strictViolations) {
        this.strictViolations = strictViolations;
    }

    public Float getCoverageLimit() {
        return coverageLimit;
    }

    public void setCoverageLimit(Float coverageLimit) {
        this.coverageLimit = coverageLimit;
    }

    public Integer getBlockerLimit() {
        return blockerLimit;
    }

    public void setBlockerLimit(Integer blockerLimit) {
        this.blockerLimit = blockerLimit;
    }

    public Integer getCriticalLimit() {
        return criticalLimit;
    }

    public void setCriticalLimit(Integer criticalLimit) {
        this.criticalLimit = criticalLimit;
    }
}
