package ch.mobi.mobitor.plugin.dwh;

/*-
 * §
 * mobitor-plugin-dwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.dwh.config.DwhConfig;
import ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentsInformation;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.jetbrains.annotations.TestOnly;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.dwhDeployments.enabled", havingValue = "true")
public class DwhDeploymentsPlugin implements MobitorPlugin<DwhConfig> {

    private final EnvironmentsConfigurationService environmentsConfigurationService;

    @Autowired
    public DwhDeploymentsPlugin(EnvironmentsConfigurationService environmentsConfigurationService) {
        this.environmentsConfigurationService = environmentsConfigurationService;
    }

    @Override
    public String getConfigPropertyName() {
        return "dwhDeployments";
    }

    @Override
    public Class<DwhConfig> getConfigClass() {
        return DwhConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<DwhConfig> configs) {
        List<String> environments = screen.getEnvironments();
        for (DwhConfig config : configs) {
            for (String env : environments) {
                if (environmentsConfigurationService.isNetworkReachable(env)) {
                    String serverName = config.getServerName();
                    for (Map.Entry<String, String> appAndPlugin : config.getAppsAndPlugins()) {
                        DwhDeploymentsInformation dwhInfo = createDwhDeploymentsInformation(serverName,appAndPlugin.getKey(), appAndPlugin.getValue(), env);
                        screen.addInformation(serverName, appAndPlugin.getKey(), env, dwhInfo);
                    }
                }
            }
        }
    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new DwhDeploymentLegendGenerator().getLegendList();
    }

    @TestOnly
    DwhDeploymentsInformation createDwhDeploymentsInformation(String serverName,String application, String plugin, String env) {
        return new DwhDeploymentsInformation(serverName, application, plugin, env);
    }
}
