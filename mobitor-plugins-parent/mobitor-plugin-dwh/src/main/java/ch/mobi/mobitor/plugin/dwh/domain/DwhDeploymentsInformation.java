package ch.mobi.mobitor.plugin.dwh.domain;

/*-
 * §
 * mobitor-plugin-dwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.information.VersionInformation;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.util.List;

import static java.util.Collections.emptyList;

public class DwhDeploymentsInformation implements ApplicationInformation, VersionInformation {
    public static final String DWH_DEPLOYMENT = "dwh_deployment";

    private final String serverName;
    private final String applicationName;
    private final String plugin;
    private final String environment;
    private List<DwhDeploymentInformation> deployments = emptyList();

    public DwhDeploymentsInformation(String serverName, String applicationName, String plugin, String environment) {
        this.serverName = serverName;
        this.applicationName = applicationName;
        this.plugin = plugin;
        this.environment = environment;
    }

    @Override
    public String getType() {
        return DWH_DEPLOYMENT;
    }

    @Override
    public boolean hasInformation() {
        return !deployments.isEmpty();
    }

    @Override
    public String getVersion() {
        return hasInformation() ? deployments.get(0).getVersion() : "";
    }

    public boolean isSuccessful() {
        return hasInformation() && deployments.get(0).isSuccessful();
    }

    public String getServerName() {
        return serverName;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public String getPlugin() {
        return plugin;
    }

    public String getEnvironment() {
        return environment;
    }

    public List<DwhDeploymentInformation> getDeployments() {
        return deployments;
    }

    public void setDeployments(List<DwhDeploymentInformation> deployments) {
        this.deployments = deployments;
    }
}
