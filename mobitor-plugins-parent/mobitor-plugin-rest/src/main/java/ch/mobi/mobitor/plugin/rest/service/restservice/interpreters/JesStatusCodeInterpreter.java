package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.service.restservice.SwaggerEndpointInterpreter;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

import static ch.mobi.mobitor.plugin.rest.domain.RestInformationUtils.success;
import static ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse.createErrorRestServiceResponse;
import static ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse.createSuccessForFastRestServiceResponse;
import static ch.mobi.mobitor.plugin.rest.service.RestPluginConstants.CACHE_NAME_REST_RESPONSES;

@Component
public class JesStatusCodeInterpreter implements SwaggerEndpointInterpreter {

    private static final Logger LOG = LoggerFactory.getLogger(JesStatusCodeInterpreter.class);

    private final RestServiceHttpRequestExecutor restServiceHttpRequestExecutor;

    @Autowired
    public JesStatusCodeInterpreter(@Qualifier("anonymous") RestServiceHttpRequestExecutor restServiceHttpRequestExecutor) {
        this.restServiceHttpRequestExecutor = restServiceHttpRequestExecutor;
    }

    @Override
    public Predicate<String> getMatchPredicate() {
        Predicate<String> ping = path -> path.contains("/ping");
        Predicate<String> readiness = path -> path.contains("/jap/readiness");
        Predicate<String> liveness = path -> path.contains("/jap/liveness");

        return ping.or(readiness).or(liveness);
    }

    @Override
    @Cacheable(cacheNames = CACHE_NAME_REST_RESPONSES)
    public RestServiceResponse fetchResponse(String uri) {
        try {
            long start = System.currentTimeMillis();
            HttpResponse httpResponse = restServiceHttpRequestExecutor.execute(uri);
            long stop = System.currentTimeMillis();
            long durationMs = stop - start;
            int statusCode = httpResponse.getStatusLine().getStatusCode();

            if (success(statusCode)) {
                return createSuccessForFastRestServiceResponse(uri, statusCode, durationMs);
            } else {
                String entity = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
                return createErrorRestServiceResponse(uri, statusCode, entity);
            }

        } catch (Exception e) {
            String actMsg = "Could not query uri: " + uri;
            String exMsg = ExceptionUtils.getStackTrace(e);
            LOG.error(actMsg, e);
            return createErrorRestServiceResponse(uri, -50, actMsg, exMsg);
        }
    }

}
