package ch.mobi.mobitor.plugin.rest.service.scheduling;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.rest.RestResourcePlugin;
import ch.mobi.mobitor.plugin.rest.domain.RestCallAdditionalUri;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.swagger.Server;
import ch.mobi.mobitor.plugin.rest.domain.swagger.SwaggerResponse;
import ch.mobi.mobitor.plugin.rest.service.RestPluginConstants;
import ch.mobi.mobitor.plugin.rest.service.client.SwaggerClient;
import ch.mobi.mobitor.plugin.rest.service.restservice.AdditionalEndpointInterpreter;
import ch.mobi.mobitor.plugin.rest.service.restservice.SwaggerEndpointInterpreter;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.client.HttpResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.function.Predicate;

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;
import static ch.mobi.mobitor.plugin.rest.domain.RestInformationUtils.getMatchingPaths;
import static ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse.createErrorRestServiceResponse;
import static java.util.function.Predicate.not;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Component
@ConditionalOnBean(RestResourcePlugin.class)
public class RestInformationCollector {

    private final static Logger LOG = LoggerFactory.getLogger(RestInformationCollector.class);

    private final ScreensModel screensModel;
    private final RuleService ruleService;
    private final RestCallMetricService restCallMetricService;
    private final CollectorMetricService collectorMetricService;
    private final SwaggerClient swaggerClient;
    private final List<SwaggerEndpointInterpreter> swaggerEndpointInterpreters;
    private final List<AdditionalEndpointInterpreter> additionalEndpointInterpreters;

    @Autowired
    public RestInformationCollector(ScreensModel screensModel,
                                    RuleService ruleService,
                                    RestCallMetricService restCallMetricService,
                                    CollectorMetricService collectorMetricService,
                                    SwaggerClient swaggerClient,
                                    List<SwaggerEndpointInterpreter> swaggerEndpointInterpreters,
                                    List<AdditionalEndpointInterpreter> additionalEndpointInterpreters) {
        this.screensModel = screensModel;
        this.ruleService = ruleService;
        this.restCallMetricService = restCallMetricService;
        this.collectorMetricService = collectorMetricService;
        this.swaggerClient = swaggerClient;
        this.swaggerEndpointInterpreters = swaggerEndpointInterpreters;
        this.additionalEndpointInterpreters = additionalEndpointInterpreters;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.restServicePollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.first}")
    @CacheEvict(cacheNames = RestPluginConstants.CACHE_NAME_REST_RESPONSES, allEntries = true)
    public void collectRestCallsServiceResponses() {
        long startTime = System.currentTimeMillis();
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateRestServiceStatusFromSwagger);
        long duration = System.currentTimeMillis() - startTime;
        LOG.info("Polling REST services took: " + duration + "ms");
        collectorMetricService.submitCollectorDuration("rest", duration);
        collectorMetricService.updateLastRunCompleted(REST);
    }

    private void populateRestServiceStatusFromSwagger(Screen screen) {
        long startTime = System.currentTimeMillis();
        List<RestCallInformation> restInfoList = screen.getMatchingInformation(REST);
        restInfoList.forEach(this::populateRestCallInformation);
        long duration = System.currentTimeMillis() - startTime;
        LOG.debug("Refreshing REST status for screen '" + screen.getLabel() + "' took: " + duration + "ms");
        screen.setRefreshDate(REST, new Date());
        ruleService.updateRuleEvaluation(screen, REST);
    }

    private void populateRestCallInformation(RestCallInformation restCallInformation) {
        List<RestServiceResponse> allRestResponses = new ArrayList<>();
        // load swagger.json
        String swaggerUri = restCallInformation.getSwaggerUri();
        SwaggerResponse swaggerResponse = null;
        if (isNotEmpty(swaggerUri)) {
            try {
                swaggerResponse = swaggerClient.retrieveSwaggerJson(swaggerUri);
                String restBasePath = swaggerClient.buildBaseUrl(swaggerUri, getBasePath(swaggerResponse));
                getAllRestPathsPerInterpreterAndCallItsMatchingMethods(restCallInformation, allRestResponses, swaggerResponse, restBasePath);
            } catch (IOException | URISyntaxException e) {
                LOG.debug("Failed to call rest service", e);
                if (e instanceof HttpResponseException) {
                    HttpResponseException ex = (HttpResponseException) e;
                    RestServiceResponse swaggerErrorResponse = createErrorRestServiceResponse(swaggerUri, ex.getStatusCode(), ex.getReasonPhrase());
                    allRestResponses.add(swaggerErrorResponse);
                } else {
                    RestServiceResponse swaggerErrorResponse = createErrorRestServiceResponse(swaggerUri, 444, "Could not load swagger.json");
                    allRestResponses.add(swaggerErrorResponse);
                }
            }
        }

        addAdditionalResponses(restCallInformation, allRestResponses);

        if (isNotEmpty(swaggerUri)) {
            Set<String> tkNameIds = extractTkNameIds(allRestResponses);
            String tkNameId = tkNameIds.stream().findFirst().orElse(swaggerResponse == null ? "!" + swaggerUri : "!" + getBasePath(
                    swaggerResponse));
            restCallInformation.setTkNameId(tkNameId);
        }

        restCallInformation.setRestServiceResponses(allRestResponses);
        restCallInformation.setRefreshDate(new Date());
        LOG.debug("Number of REST paths found: " + allRestResponses.size());

        restCallMetricService.submitRestCallFailures(restCallInformation);
    }

    private String getBasePath(SwaggerResponse swaggerResponse) {
        String basePath = null;
        if (swaggerResponse.getSwaggerVersion() != null) {
            basePath = swaggerResponse.getBasePath();
        } else if (swaggerResponse.getOpenApiVersion() != null) {
            if (swaggerResponse.getServers() != null) {
                basePath = swaggerResponse.getServers().stream().map(Server::getUrl).filter(not(Objects::isNull)).findFirst()
                        // In the OpenApi 3 format the server URL is complete and it is not only the basePath as in Swagger2 format
                        .map(this::extractBasePathFromUrl)
                        .orElse(null);
            }
        } else {
            LOG.debug("Invalid OpenAPI response: neither 'swaggerVersion' nor 'openApiVersion' is set");
            // assume Swagger/OpenAPI 2 for compatibility
            basePath = swaggerResponse.getBasePath();
        }
        if (basePath == null) {
            basePath = "";
        }
        return basePath;
    }

    private String extractBasePathFromUrl(String path) {
        String basePath = "";
        if (path.contains("http")) {
            try {
                URL url = new URL(path);
                basePath = path.replace(url.getProtocol(), "");
                basePath = basePath.replace(url.getHost(), "");
            } catch (MalformedURLException e) {
                basePath = path;
            }
        } else {
            basePath = path;
        }
        return basePath;
    }

    private void getAllRestPathsPerInterpreterAndCallItsMatchingMethods(RestCallInformation restCallInformation, List<RestServiceResponse> allRestResponses, SwaggerResponse swaggerResponse, String restBasePath) {
        for (SwaggerEndpointInterpreter interpreter : swaggerEndpointInterpreters) {
            Predicate<String> matchPredicate = interpreter.getMatchPredicate();
            List<String> matchingPaths = getMatchingPaths(restBasePath, matchPredicate, swaggerResponse.getPaths());

            matchingPaths.forEach(path -> {
                RestServiceResponse restServiceResponse = interpreter.fetchResponse(path);
                allRestResponses.add(restServiceResponse);

                Optional.ofNullable(restServiceResponse.getProperty("stackName")).ifPresent(restCallInformation::setStackName);
                Optional.ofNullable(restServiceResponse.getProperty("stackVersion")).ifPresent(restCallInformation::setStackVersion);
            });
        }
    }

    private void addAdditionalResponses(RestCallInformation restCallInformation, List<RestServiceResponse> allRestResponses) {
        List<RestCallAdditionalUri> additionalUris = restCallInformation.getAdditionalUris();
        if (CollectionUtils.isNotEmpty(additionalUris)) {
            for (AdditionalEndpointInterpreter interpreter : additionalEndpointInterpreters) {
                additionalUris.forEach(additionalUri -> {
                    RestServiceResponse restServiceResponse = interpreter.fetchResponse(additionalUri.getUri(), additionalUri.getValidationRegex());
                    LOG.debug(restServiceResponse.toString());
                    allRestResponses.add(restServiceResponse);
                });
            }
        }
    }

    private Set<String> extractTkNameIds(List<RestServiceResponse> allSwaggerRestResponses) {
        Set<String> tkNameIds = new HashSet<>();
        allSwaggerRestResponses.forEach(restResponse -> {
            if (restResponse.getProperty("tkNameId") != null) {
                tkNameIds.add(restResponse.getProperty("tkNameId"));
            }
        });

        if (CollectionUtils.size(tkNameIds) > 1) {
            LOG.error("Different tkNameIds reported: " + tkNameIds);
        }
        return tkNameIds;
    }

}
