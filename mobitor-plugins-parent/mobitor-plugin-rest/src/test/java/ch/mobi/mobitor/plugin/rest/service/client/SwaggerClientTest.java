package ch.mobi.mobitor.plugin.rest.service.client;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import ch.mobi.mobitor.plugin.rest.domain.swagger.SwaggerResponse;
import org.apache.http.client.HttpResponseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;

public class SwaggerClientTest {

    private SwaggerClient swaggerClient;

    @BeforeEach
    public void setup() {
        RestPluginConfiguration restPluginConfiguration = new RestPluginConfiguration();
        restPluginConfiguration.setVersion("0.0.0-junit");
        swaggerClient = spy(new SwaggerClient(restPluginConfiguration, "username"));
    }

    @Test
    public void buildBaseUrl() {
        verifySwaggerBaseUrl("https://app.host.domain/ovn/rest",
                             "https://app.host.domain/ovn/swagger.json",
                             "/ovn/rest");

        verifySwaggerBaseUrl("http://app-test.host.domain/vvn/angebot/rest",
                             "http://app-test.host.domain/vvn/angebot/rest/swagger.json",
                             "/vvn/angebot/rest");
    }

    private void verifySwaggerBaseUrl(String expectedRestPath, String swaggerJsonUrl, String basePath) {
        String restBaseUrl = swaggerClient.buildBaseUrl(swaggerJsonUrl, basePath);

        assertEquals(expectedRestPath, restBaseUrl);
    }

    @Test
    public void rethrowHttpResponseException() throws Exception {
        //arrange
        doThrow(new HttpResponseException(418, "I'm a teapot")).when(swaggerClient).executeGetRequest(any());

        //act
        Throwable thrown = catchThrowable(() -> swaggerClient.retrieveSwaggerJson(""));

        //assert
        assertThat(thrown).isInstanceOfSatisfying(HttpResponseException.class, e -> assertThat(e.getStatusCode()).isEqualTo(418));
    }

    @Test
    public void rethrowURISyntaxException() throws Exception {
        //arrange
        doThrow(new URISyntaxException("", "")).when(swaggerClient).executeGetRequest(any());

        //act
        Throwable thrown = catchThrowable(() -> swaggerClient.retrieveSwaggerJson(""));

        //assert
        assertThat(thrown).isInstanceOf(URISyntaxException.class);
    }

    @Test
    public void returnSwaggerResponseForSuccessfulRequests() throws Exception {
        //arrange
        doReturn("{}").when(swaggerClient).executeGetRequest(any());

        //act
        SwaggerResponse response = swaggerClient.retrieveSwaggerJson("");

        //assert
        assertThat(response).isNotNull();
    }
}
