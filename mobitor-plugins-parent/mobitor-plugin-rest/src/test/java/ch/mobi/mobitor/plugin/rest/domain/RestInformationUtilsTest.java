package ch.mobi.mobitor.plugin.rest.domain;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.swagger.PathHttpMethods;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static ch.mobi.mobitor.plugin.rest.domain.RestInformationUtils.success;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class RestInformationUtilsTest {

    @Test
    public void getMatchingGetPaths() {
        // arrange
        Predicate<String> healthStatusPredicate = path -> path.contains("/health/status");
        Map<String, PathHttpMethods> paths = new HashMap<>();

        PathHttpMethods getMethods = new PathHttpMethods();
        getMethods.add("get", null);

        paths.put("/rest/health/status", getMethods);
        paths.put("/rest/ping/pong", getMethods);

        // act
        List<String> matchingPaths = RestInformationUtils.getMatchingPaths("/rest/ovn/", healthStatusPredicate, paths);

        // assert
        assertThat(matchingPaths).hasSize(1);
    }

    @Test
    public void getMatchingPathsWithOnlyPostPaths() {
        // arrange
        Predicate<String> healthStatusPredicate = path -> path.contains("/health/status");
        Map<String, PathHttpMethods> paths = new HashMap<>();

        PathHttpMethods getMethods = new PathHttpMethods();
        getMethods.add("post", null);

        paths.put("/rest/health/status", getMethods);
        paths.put("/rest/ping/pong", getMethods);

        // act
        List<String> matchingPaths = RestInformationUtils.getMatchingPaths("/rest/ovn/", healthStatusPredicate, paths);

        // assert
        assertThat(matchingPaths).hasSize(0);
    }

    @Test
    public void normalizeUriWithDoubleSlashes() {
        // arrange
        String constructedUrl = "http://url.com//tada";

        // act
        String normalizeUri = RestInformationUtils.normalizeUri(constructedUrl);

        // assert
        assertThat(normalizeUri).isEqualTo("http://url.com/tada");
    }

    @Test
    public void normalizeInvalidUri() {
        // arrange
        String constructedUrl = "url.//\\\\//.com//tada";

        // act
        String normalizeUri = RestInformationUtils.normalizeUri(constructedUrl);

        // assert
        assertThat(normalizeUri).isNull();
    }

    @Test
    public void httpSuccessTest() {
        assertThat(success(200)).isTrue();
        assertThat(success(299)).isTrue();
    }

    @Test
    public void httpFailureTest() {
        assertThat(success(199)).isFalse();
        assertThat(success(300)).isFalse();
        assertThat(success(-1)).isFalse();
        assertThat(success(500)).isFalse();
    }

}
