package ch.mobi.mobitor.plugin.edwh.service.client;

/*-
 * §
 * mobitor-plugin-edwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.edwh.service.client.dto.EdwhServerConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class EdwhConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(EdwhConfigurationService.class);

    private final ResourceLoader resourceLoader;
    private Map<String, EdwhServerConfig> edwhServerConfigMap;

    @Autowired
    public EdwhConfigurationService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @PostConstruct
    public void initializeEdwhServers() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        Resource resource = resourceLoader.getResource("classpath:mobitor/plugin/edwh/config/dwh-etl-servers.json");
        try (InputStream inputStream = resource.getInputStream()) {
            List<EdwhServerConfig> edwhServerConfigs = mapper.readValue(inputStream, mapper.getTypeFactory().constructCollectionType(List.class, EdwhServerConfig.class));
            edwhServerConfigMap = edwhServerConfigs.stream().collect(Collectors.toMap(EdwhServerConfig::getEnvironment, Function.identity()));

        } catch (Exception e) {
            LOG.error("Could not initialize kubernetes servers.", e);
        }
    }

    public EdwhServerConfig getEdwhServerConfig(String environment) {
        return edwhServerConfigMap.get(environment);
    }

}
