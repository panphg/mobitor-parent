package ch.mobi.mobitor.plugin.edwh;

/*-
 * §
 * mobitor-plugin-edwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.edwh.config.EdwhConfig;
import ch.mobi.mobitor.plugin.edwh.domain.EdwhDeploymentsInformation;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.jetbrains.annotations.TestOnly;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.edwhDeployments.enabled", havingValue = "true")
public class EdwhDeploymentsPlugin implements MobitorPlugin<EdwhConfig> {

    private final EnvironmentsConfigurationService environmentsConfigurationService;

    @Autowired
    public EdwhDeploymentsPlugin(EnvironmentsConfigurationService environmentsConfigurationService) {
        this.environmentsConfigurationService = environmentsConfigurationService;
    }

    @Override
    public String getConfigPropertyName() {
        return "edwhDeployments";
    }

    @Override
    public Class<EdwhConfig> getConfigClass() {
        return EdwhConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<EdwhConfig> configs) {
        List<String> environments = screen.getEnvironments();
        for (EdwhConfig edwhConfig : configs) {
            for (String env : environments) {
                if (environmentsConfigurationService.isNetworkReachable(env)) {
                    String serverName = edwhConfig.getServerName();
                    for (String appName : edwhConfig.getApplicationNames()) {
                        EdwhDeploymentsInformation edwhInfo = createEdwhDeploymentsInformation(serverName, appName, env);
                        screen.addInformation(serverName, appName, env, edwhInfo);
                    }
                }
            }
        }
    }

    @TestOnly
    EdwhDeploymentsInformation createEdwhDeploymentsInformation(String serverName, String applicationName, String env) {
        EdwhDeploymentsInformation edwhInfo = new EdwhDeploymentsInformation();
        edwhInfo.setServerName(serverName);
        edwhInfo.setApplicationName(applicationName);
        edwhInfo.setEnvironment(env);
        return edwhInfo;
    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new EdwhDeploymentLegendGenerator().getLegendList();
    }
}
