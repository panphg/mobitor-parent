package ch.mobi.mobitor.plugin.edwh.rule;

/*-
 * §
 * mobitor-plugin-edwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.domain.screen.RuleViolationSeverity;
import ch.mobi.mobitor.domain.screen.ServerContext;
import ch.mobi.mobitor.plugin.edwh.domain.EdwhDeploymentsInformation;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static ch.mobi.mobitor.plugin.edwh.domain.EdwhDeploymentsInformation.EDWH_DEPLOYMENT;

@Component
public class EdwhDeploymentSuccessRule implements PipelineRule {

    @Override
    public void evaluateRule(Pipeline pipeline, RuleEvaluation newRuleEvaluation) {
        Map<String, ServerContext> serverContextMap = pipeline.getServerContextMap();
        for (Map.Entry<String, ServerContext> entry : serverContextMap.entrySet()) {
            String env = entry.getKey();
            ServerContext serverContext = entry.getValue();

            List<EdwhDeploymentsInformation> edwhDeploymentsInformationList = serverContext.getMatchingInformation(EDWH_DEPLOYMENT);
            edwhDeploymentsInformationList.stream().filter(this::ruleViolated).forEach(edwhDeploymentsInformation -> newRuleEvaluation.addViolation(env, edwhDeploymentsInformation, RuleViolationSeverity.ERROR, "EDWH Deployment unhappy."));
        }
    }

    private boolean ruleViolated(EdwhDeploymentsInformation edwhDeploymentsInformation) {
        return !edwhDeploymentsInformation.isSuccessful();
    }

    @Override
    public boolean validatesType(String type) {
        return EDWH_DEPLOYMENT.equals(type);
    }
}
