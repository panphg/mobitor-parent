package ch.mobi.mobitor.plugin.edwh.service.client;

/*-
 * §
 * mobitor-plugin-edwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

@Service
public class EdwhDeploymentsService {

    private static final Logger LOG = LoggerFactory.getLogger(EdwhDeploymentsService.class);

    private final EdwhConfigurationService configurationService;

    @Autowired
    public EdwhDeploymentsService(EdwhConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Nullable
    public EdwhDeployment retrieveDeploymentInformation(String serverName, String applicationName, String environment) {
        String url = configurationService.getEdwhServerConfig(environment).getUrl() + "/releases/" + serverName;
        try {
            WebClient client = WebClient.create();

            Mono<EdwhDeployment[]> edwhDeploymentsMono = client.get()
                                                              .uri(url)
                                                              .accept(MediaType.APPLICATION_JSON)
                                                              .retrieve()
                                                              .bodyToMono(EdwhDeployment[].class);

            EdwhDeployment[] edwhDeployments = edwhDeploymentsMono.block();
            List<EdwhDeployment> matchingList = null;
            if (edwhDeployments != null) {
                matchingList = stream(edwhDeployments).filter(d -> applicationName.equals(d.getDeployObjekt())).collect(Collectors.toList());
            }
            if (matchingList != null && matchingList.size() == 1) {
                return matchingList.get(0);
            }

        } catch (Exception ex) {
            LOG.error("Could not read deployments from: " + url, ex);
        }
        return null;
    }

}
