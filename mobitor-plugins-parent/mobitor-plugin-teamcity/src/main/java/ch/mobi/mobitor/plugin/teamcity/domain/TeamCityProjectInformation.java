package ch.mobi.mobitor.plugin.teamcity.domain;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

public class TeamCityProjectInformation implements ApplicationInformation {

    public static final String TEAM_CITY_PROJECT = "team_city_project";

    private final String projectId;

    private final String label;

    private String number;

    private String status;

    private String webUrl;

    private String id;

    private int jobsTotal;

    private int jobsPassed;

    private int jobsFailed;

    private boolean showAnyBranch = false;

    private boolean running;

    private int percentageComplete;

    public TeamCityProjectInformation(String projectId, String label) {
        this.projectId = projectId;
        this.label = label;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String getType() {
        return TEAM_CITY_PROJECT;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public boolean isSuccessful() {
        return "SUCCESS".equals(getStatus());
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public void setJobsTotal(int jobsTotal) {
        this.jobsTotal = jobsTotal;
    }

    public int getJobsTotal() {
        return jobsTotal;
    }

    public void setJobsPassed(int jobsPassed) {
        this.jobsPassed = jobsPassed;
    }

    public int getJobsPassed() {
        return jobsPassed;
    }

    public void setJobsFailed(int jobsFailed) {
        this.jobsFailed = jobsFailed;
    }

    public int getJobsFailed() {
        return jobsFailed;
    }

    public boolean isShowAnyBranch() {
        return showAnyBranch;
    }

    public void setShowAnyBranch(boolean showAnyBranch) {
        this.showAnyBranch = showAnyBranch;
    }

    public void setPercentageComplete(int percentageComplete) {
        this.percentageComplete = percentageComplete;
    }

    public int getPercentageComplete() {
        return percentageComplete;
    }
}
