package ch.mobi.mobitor.plugin.teamcity.domain;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.util.Date;

public class TeamCityBuildInformation implements ApplicationInformation {

    public static final String TEAM_CITY_BUILD = "team_city_build";


    private final String configId;
    private final String label;
    private Date updated;
    private String state; // running
    private String number;
    private String status; // SUCCESS
    private String webUrl;
    private int percentageComplete;
    private String id;
    private int testsTotal;
    private int testsPassed;
    private int testsFailed;
    private int testsIgnored;
    private boolean showAnyBranch = false;

    public TeamCityBuildInformation(String configId, String label) {
        this.configId = configId;
        this.label = label;
    }

    public String getConfigId() {
        return configId;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String getType() {
        return TEAM_CITY_BUILD;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setPercentageComplete(int percentageComplete) {
        this.percentageComplete = percentageComplete;
    }

    public int getPercentageComplete() {
        return percentageComplete;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public boolean isSuccessful() {
        return "SUCCESS".equals(getStatus());
    }

    public boolean isRunning() {
        return "running".equals(getState());
    }

    public void setTestsTotal(int testsTotal) {
        this.testsTotal = testsTotal;
    }

    public int getTestsTotal() {
        return testsTotal;
    }

    public void setTestsPassed(int testsPassed) {
        this.testsPassed = testsPassed;
    }

    public int getTestsPassed() {
        return testsPassed;
    }

    public void setTestsFailed(int testsFailed) {
        this.testsFailed = testsFailed;
    }

    public int getTestsFailed() {
        return testsFailed;
    }

    public void setTestsIgnored(int testsIgnored) {
        this.testsIgnored = testsIgnored;
    }

    public int getTestsIgnored() {
        return testsIgnored;
    }

    public boolean isShowAnyBranch() {
        return showAnyBranch;
    }

    public void setShowAnyBranch(boolean showAnyBranch) {
        this.showAnyBranch = showAnyBranch;
    }

}
