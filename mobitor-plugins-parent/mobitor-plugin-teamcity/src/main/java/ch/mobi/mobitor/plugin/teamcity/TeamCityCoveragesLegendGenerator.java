package ch.mobi.mobitor.plugin.teamcity;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 - 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityCoverageInformation;
import ch.mobi.mobitor.plugins.api.MobitorPluginLegendGenerator;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;

import java.math.BigDecimal;
import java.util.List;

import static java.util.Collections.singletonList;

public class TeamCityCoveragesLegendGenerator implements MobitorPluginLegendGenerator {

    @Override
    public List<ApplicationInformationLegendWrapper> createSuccessList() {
        TeamCityCoverageInformation tcGoodCoverage = new TeamCityCoverageInformation("succConfId", "RC");
        tcGoodCoverage.setStatus("SUCCESS");
        tcGoodCoverage.setCoverage(new BigDecimal(97.2));

        ApplicationInformationLegendWrapper successDeployment = new ApplicationInformationLegendWrapper("Good coverage.", tcGoodCoverage);
        return singletonList(successDeployment);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createErrorList() {
        TeamCityCoverageInformation tcBadCoverage = new TeamCityCoverageInformation("errConfId", "RC");
        tcBadCoverage.setStatus("SUCCESS");
        tcBadCoverage.setCoverage(new BigDecimal(12.7));

        ApplicationInformationLegendWrapper successDeployment = new ApplicationInformationLegendWrapper("Bad coverage.", tcBadCoverage);
        return singletonList(successDeployment);
    }
}
