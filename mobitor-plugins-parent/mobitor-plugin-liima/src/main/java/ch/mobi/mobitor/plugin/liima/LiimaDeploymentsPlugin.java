package ch.mobi.mobitor.plugin.liima;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.liima.config.AppServerConfig;
import ch.mobi.mobitor.plugin.liima.domain.LiimaInformation;
import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Component
@ConditionalOnProperty(name = "mobitor.plugins.liimaServers.enabled", havingValue = "true")
public class LiimaDeploymentsPlugin implements MobitorPlugin<String> {

    private static final Logger LOG = LoggerFactory.getLogger(LiimaDeploymentsPlugin.class);

    private final ServersConfigurationService serversConfigurationService;

    @Autowired
    public LiimaDeploymentsPlugin(ServersConfigurationService serversConfigurationService) {
        this.serversConfigurationService = serversConfigurationService;
    }

    @Override
    public String getConfigPropertyName() {
        return "liimaServers";
    }

    @Override
    public Class<String> getConfigClass() {
        return String.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<String> configs) {
        // List<String> appServerNames = screenConfig.getPluginConfigMap().get(getConfigPropertyName()); // equals to 'configs' parameter
        Map<String, AppServerConfig> appServerNameToConfigMap = serversConfigurationService.getAppServerNameToConfigMap();

        for (String serverName : configs) {
            AppServerConfig appServerConfig = appServerNameToConfigMap.get(serverName);
            if (appServerConfig != null) {
                for (String environment : screenConfig.getEnvironments()) {
                    List<String> applicationNames = appServerConfig.getApplicationNames();
                    for (String applicationName : applicationNames) {
                        LiimaInformation liimaInfo = new LiimaInformation(serverName, applicationName, environment);
                        screen.addInformation(serverName, applicationName, environment, liimaInfo);
                    }
                }
            } else {
                LOG.error("There is no configuration for appServer: " + serverName);
            }
        }
    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new LiimaDeploymentsLegendGenerator().getLegendList();
    }

}
