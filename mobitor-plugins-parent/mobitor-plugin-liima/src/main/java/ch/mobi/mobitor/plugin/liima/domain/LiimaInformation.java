package ch.mobi.mobitor.plugin.liima.domain;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.domain.screen.information.VersionInformation;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

public class LiimaInformation implements ApplicationInformation, VersionInformation {

    public static final String LIIMA = "liima";

    private final String serverName;
    private final String applicationName;
    private final String environment;
    private String version;
    private Long deploymentDate;
    private String state;
    private String trackingId;
    private String liimaDeploymentWebUri;

    public LiimaInformation(String serverName, String applicationName, String environment) {
        this.serverName = serverName;
        this.applicationName = applicationName;
        this.environment = environment;
    }

    @Override
    public String getType() {
        return LIIMA;
    }

    @Override
    public boolean hasInformation() {
        return version != null;
    }

    @Override
    public String getVersion() {
        return version;
    }

    public String getServerName() {
        return serverName;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setDeploymentDate(Long deploymentDate) {
        this.deploymentDate = deploymentDate;
    }

    public Long getDeploymentDate() {
        return deploymentDate;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public String getLiimaDeploymentWebUri() {
        return liimaDeploymentWebUri;
    }

    public void setLiimaDeploymentWebUri(String liimaDeploymentWebUri) {
        this.liimaDeploymentWebUri = liimaDeploymentWebUri;
    }
}
