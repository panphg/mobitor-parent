package ch.mobi.mobitor.plugin.liima;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.liima.domain.LiimaInformation;
import ch.mobi.mobitor.plugins.api.MobitorPluginLegendGenerator;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;

import java.util.List;

import static java.util.Collections.singletonList;

public class LiimaDeploymentsLegendGenerator implements MobitorPluginLegendGenerator {

    @Override
    public List<ApplicationInformationLegendWrapper> createSuccessList() {
        LiimaInformation liimaDeploymentInfo = new LiimaInformation("server_name", "application_name", "test");
        liimaDeploymentInfo.setState("success");
        liimaDeploymentInfo.setVersion("1.2.3");

        ApplicationInformationLegendWrapper successDeployment = new ApplicationInformationLegendWrapper("Successful deployment.", liimaDeploymentInfo);
        return singletonList(successDeployment);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createErrorList() {
        LiimaInformation liimaDeploymentInfo = new LiimaInformation("server_name", "application_name", "test");
        liimaDeploymentInfo.setState("failed");
        liimaDeploymentInfo.setVersion("1.2.3");

        ApplicationInformationLegendWrapper successDeployment = new ApplicationInformationLegendWrapper("Failed deployment.", liimaDeploymentInfo);
        return singletonList(successDeployment);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createProgressList() {
        LiimaInformation liimaDeploymentInfo = new LiimaInformation("server_name", "application_name", "test");
        liimaDeploymentInfo.setState("progress");
        liimaDeploymentInfo.setVersion("1.2.3");

        ApplicationInformationLegendWrapper successDeployment = new ApplicationInformationLegendWrapper("Deployment in progress.", liimaDeploymentInfo);
        return singletonList(successDeployment);
    }
}
