package ch.mobi.mobitor.plugin.bitbucket.service.client;

/*-
 * §
 * mobitor-plugin-bitbucket
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.bitbucket.BitBucketPluginConfiguration;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketCommitInfoResponse;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketCommitResponse;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketTagResponse;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.matching.AnythingPattern;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.cloud.contract.wiremock.WireMockSpring.options;

public class BitBucketClientTest {

    public static WireMockServer wiremock = new WireMockServer(options().dynamicPort());

    private BitBucketPluginConfiguration bitBucketPluginConfiguration = new BitBucketPluginConfiguration();
    private BitBucketClient client;

    @BeforeAll
    static void setup() {
        wiremock.start();
    }

    @BeforeEach
    public void setupBitBucketConfig() {
        String baseUrl = "http://localhost:" + wiremock.port();

        bitBucketPluginConfiguration.setUsername("bitbucket-junit");
        bitBucketPluginConfiguration.setPassword("bitbucket-junit");
        bitBucketPluginConfiguration.setBaseUrl(baseUrl);

        client = new BitBucketClient(bitBucketPluginConfiguration);
    }

    @AfterEach
    void after() {
        wiremock.resetAll();
    }

    @AfterAll
    static void clean() {
        wiremock.shutdown();
    }

    private void configureTagsUrl() {
        wiremock.stubFor(get(urlMatching("/rest/api/1.0/projects/([a-zA-Z0-9_\\-]+)/repos/([a-zA-Z0-9_\\-]+)/tags/.*"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("filterText", new AnythingPattern())
                .withBasicAuth(bitBucketPluginConfiguration.getUsername(), bitBucketPluginConfiguration.getPassword())
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("bitbucket-tags-list.json")
                )
        );
    }

    private void configureCommitIdUrl() {
        wiremock.stubFor(get(urlMatching("/rest/api/1.0/projects/([a-zA-Z0-9_\\-]+)/repos/([a-zA-Z0-9_\\-]+)/commits/([a-zA-Z0-9_\\-]{40})"))
                .withHeader("Accept", equalTo("application/json"))
                .withBasicAuth(bitBucketPluginConfiguration.getUsername(), bitBucketPluginConfiguration.getPassword())
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("bitbucket-commit-9e4a22.json")
                )
        );
    }

    private void configureCommitIdsUrl() {
        wiremock.stubFor(get(urlMatching("/rest/api/1.0/projects/([a-zA-Z0-9_\\-]+)/repos/([a-zA-Z0-9_\\-]+)/commits/(.*)"))
                .withHeader("Accept", equalTo("application/json"))
                .withBasicAuth(bitBucketPluginConfiguration.getUsername(), bitBucketPluginConfiguration.getPassword())
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("bitbucket-commits-list.json")
                )
        );
    }

    @Test
    public void retrieveTags() {
        // arrange
        configureTagsUrl();

        // act
        List<BitBucketTagResponse> tags = client.retrieveTags("vvn", "vvn-angebot-jeeservice", "");

        // assert
        wiremock.verify(getRequestedFor(urlMatching("/rest/api/1.0/projects/([a-zA-Z0-9_\\-]+)/repos/([a-zA-Z0-9_\\-]+)/tags/.*")).withQueryParam("filterText", new AnythingPattern()));

        assertThat(tags).isNotNull();
        assertThat(tags).isNotEmpty();
    }

    @Test
    public void retrieveCommit() {
        // arrange
        configureCommitIdUrl();

        // act
        BitBucketCommitResponse commitInfo = client.retrieveCommit("vvn", "vvn-angebot-jeeservice", "72fc2749b789b2083e9de11392ae9499a24a8c9d");

        // assert
        wiremock.verify(getRequestedFor(urlMatching("/rest/api/1.0/projects/([a-zA-Z0-9_\\-]+)/repos/([a-zA-Z0-9_\\-]+)/commits/([a-zA-Z0-9_\\-]{40})")));

        assertThat(commitInfo).isNotNull();
        assertThat(commitInfo.getAuthorTimestamp()).isNotEmpty();

        assertThat(commitInfo.getAuthor()).isNotNull();
        assertThat(commitInfo.getAuthor().getDisplayName()).isNotNull();
    }

    @Test
    public void retrieveCommits() {
        // arrange
        configureCommitIdsUrl();

        // act
        List<BitBucketCommitInfoResponse> commitInfos = client.retrieveCommits("mcs", "mcs-jeeservice", "");

        // assert
        wiremock.verify(getRequestedFor(urlMatching("/rest/api/1.0/projects/([a-zA-Z0-9_\\-]+)/repos/([a-zA-Z0-9_\\-]+)/commits/(.*)")));

        assertThat(commitInfos).isNotNull();
        assertThat(commitInfos).isNotEmpty();
        for (BitBucketCommitInfoResponse commitInfo : commitInfos) {
            assertThat(commitInfo.getJiraKeys()).isNotNull();
            if ("bc87ed15b90cb2764a3cf26e79016f4a30840f27".equals(commitInfo.getId())) {
                assertThat(commitInfo.getJiraKeys()).hasSize(2);
            }
        }
    }

}
